import { Reducer } from 'redux';
import * as React from 'react';
import { ADD_CATEGORY, REMOVE_CATEGORY, RESET_GAME } from '../model/actions';

import update = require('immutability-helper');
import { findNextIndex } from '../utils/index.generator';
import { defaultState } from '../model/state';
import { Category } from '../model/domain';

export const categoriesReducer: Reducer<any> =  (state:any = [], action: any): any => {
	switch (action.type) {
		case RESET_GAME:
			return defaultState.categories;
		case ADD_CATEGORY:
			return update(state, { $push: [{
					id: findNextIndex(state),
					name: action.categoryName
				}]});
		case REMOVE_CATEGORY:
			return update(state, {$splice: [[state.findIndex((category:Category) => category.id === action.id), 1]]});
		default:
			return state;
	}
};
