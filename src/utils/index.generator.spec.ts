import { expect } from 'chai';
import { findNextIndex } from './index.generator';

describe("Index generator spec", () => {
	it('should return 1 when collection is empty', () => {
		let index: number = findNextIndex([]);

		expect(index).to.equal(1);
	});

	it('should return incremented highest index in collection as new index', () => {
		const collection: Object[] = [
			{id: 1},
			{id: 3},
			{id: 2},
			{id: 10},
			{id: 4},
			{id: 8}
		];

		const index: number = findNextIndex(collection);

		expect(index).to.equal(11);
	})
});
