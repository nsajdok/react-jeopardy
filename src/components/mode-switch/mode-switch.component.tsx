import * as React from 'react';
import { connect } from 'react-redux';
import { JoepardyState } from '../../model/domain';
import { createChangeModeAction } from '../../model/actions';

type ModeSwitchProps = {
	mode: string,
	onModeSwitch: Function
}

export const ADMIN_MODE: string = 'Admin';
export const GAME_MODE: string = 'Game';

export class ModeSwitchComponent extends React.Component<ModeSwitchProps, void> {
	render(): JSX.Element {
		let buttonText: string = 'Trigger ' + ADMIN_MODE + ' Mode';

		if (this.props.mode === ADMIN_MODE) {
			buttonText = 'Trigger ' + GAME_MODE + ' Mode';
		}

		return (
			<button onClick={this.onModeSwitch.bind(this)}>
				{buttonText}
			</button>
		);
	}

	private onModeSwitch(event: React.SyntheticEvent<HTMLButtonElement>): void {
		event.preventDefault();

		let targetMode: string = ADMIN_MODE;

		if (this.props.mode === ADMIN_MODE) {
			targetMode = GAME_MODE;
		}

		this.props.onModeSwitch(targetMode);
	}
}

const mapStateToProps: any = (state: JoepardyState): Object => {
	return {
		mode: state.mode
	}
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		onModeSwitch: (targetMode: string) => {
			dispatch(createChangeModeAction(targetMode));
		}
	}
};

export const ConnectedModeSwitchComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(ModeSwitchComponent);
