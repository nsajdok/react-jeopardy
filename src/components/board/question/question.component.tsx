import * as React from 'react';
import { Question } from '../../../model/domain';

type QuestionComponentProps = {
	question: Question,
	onQuestionSelect: Function
}

export class QuestionComponent extends React.Component<QuestionComponentProps, void> {
	render(): JSX.Element {

		if (this.props.question.answered) {
			return (
				<div className="question">
					<p className="answered">{this.props.question.price}</p>
				</div>
			)
		}

		return (
			<div className="question" onClick={this.props.onQuestionSelect.bind(this, this.props.question)}>
				<p>{this.props.question.price}</p>
			</div>
		)
	}
}
