import * as React from 'react';
import { mount } from "enzyme";
import { expect } from "chai";
import * as sinon from "sinon";

import { CategoryComponent } from "./category.component";

describe('Category Component', () => {
	it('should render category with its questions', () => {
		const props = {
			category: {
				id: 1,
				name: 'some-category',
				questions: [
					{
						id: 1,
						price: 100,
						question: 'some-question',
						answer: 'some-answer',
						answered: true,
						categoryId: 1
					}
				]
			},
			onQuestionSelect: () => {}
		};

		const wrapper = mount(<CategoryComponent {...props} />);
		const categoryName = wrapper.find('.category');
		const questions = wrapper.find('.question');

		expect(categoryName).to.have.text(props.category.name);
		expect(questions.length).to.equal(1);
	});
});
