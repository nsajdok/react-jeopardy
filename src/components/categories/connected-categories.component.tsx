import { connect } from 'react-redux';
import { CategoriesComponent, CategoryItem } from './categories.component';
import { JoepardyState, Category, Question } from '../../model/domain';
import {
	createAddCategoryAction, createRemoveCategoryAction, createAddQuestionAction,
	createRemoveQuestionAction
} from '../../model/actions';

const mapStateToProps: any = (state: JoepardyState): Object => {
	return {
		categories: state.categories.map((category: Category): CategoryItem => {
			return {
				category: category,
				questions: state.questions.filter((question: Question): boolean => {
					return question.categoryId === category.id;
				})
			}
		})
	}
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		onAddCategory: (categoryName: string) => {
			dispatch(createAddCategoryAction(categoryName))
		},
		onRemoveCategory: (categoryId: number) => {
			dispatch(createRemoveCategoryAction(categoryId));
		},
		onAddQuestion: (categoryId: number, question: {question: string, answer: string, price: number}) => {
			dispatch(createAddQuestionAction(categoryId, question));
		},
		onRemoveQuestion: (questionId: number) => {
			dispatch(createRemoveQuestionAction(questionId));
		}
	}
};

export const ConnectedCategoriesComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(CategoriesComponent);
