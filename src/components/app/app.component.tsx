import * as React from 'react';
import { ConnectedModeSwitchComponent } from '../mode-switch/mode-switch.component';
import { JoepardyState } from '../../model/domain';
import { connect } from 'react-redux';
import { ADMIN_MODE } from '../mode-switch/mode-switch.component';
import { ConnectedUsersComponent } from '../users/connected-users.component';
import { ConnectedCategoriesComponent } from '../categories/connected-categories.component';
import { ConnectedBoardComponent } from '../board/connected-board.component';
import { ConnectedActiveQuestionComponent } from '../active-question/connected-active-question.component';
import { ConnectedResetComponent } from '../reset/reset.component';


type AppProps = {
	mode: string
}

class App extends React.Component<AppProps, void> {
	render(): JSX.Element {
		let activeComponents: JSX.Element;

		if (this.props.mode === ADMIN_MODE) {
			activeComponents = (
				<div>
					<ConnectedUsersComponent />
					<ConnectedCategoriesComponent />
				</div>
			);
		} else {
			activeComponents = (
				<div>
					<ConnectedActiveQuestionComponent />
					<ConnectedBoardComponent />
				</div>
			);
		}

		return (
			<div>
				<ConnectedModeSwitchComponent />
				<ConnectedResetComponent />
				{activeComponents}
			</div>
		)
	}
}

const mapStateToProps: any = (state: JoepardyState): Object => {
	return {
		mode: state.mode
	}
};

const AppComponent = connect(mapStateToProps)(App);

export default AppComponent;

