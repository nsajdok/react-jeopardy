import * as React from 'react';

type UserComponentProps = {
	id: number,
	name: string,
	onRemoveUser: Function
}

export class UserComponent extends React.Component<UserComponentProps, void> {
	render(): JSX.Element {
		return (
			<li onClick={this.handleUserClick.bind(this)}>{this.props.name}</li>
		)
	}

	private handleUserClick() {
		if (confirm("Remove user ?")) {
			this.props.onRemoveUser(this.props.id);
		}
	}
}
