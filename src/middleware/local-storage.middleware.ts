import { saveState } from '../service/local-storage.service';
import { Middleware } from 'redux';

export const localStorageMiddleware: Middleware = ({getState}: any) => {
	return (next: any) => (action: any) => {
		let returnValue = next(action);

		saveState(getState());

		return returnValue;
	}
};
